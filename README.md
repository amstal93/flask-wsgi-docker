# Flask-wsgi-docker

## Como implantar uma API para produção com WSGI

### O que é o WSGI?

WSGI Web Server Gateway Interface (Interface de Porta de Entrada do Servidor Web) não é um servidor, um módulo python, um framework, uma API ou qualquer tipo de software. É uma especificação que descreve como um servidor da web se comunica com os aplicativos da web e como os aplicativos da web podem ser encadeados para processar uma solicitação.

Muitos frameworks oferecem suportes para WSGI e também servidores conhecidos suportam a mesma. Sua documentação do WSGI é encontrada [aqui](https://wsgi.readthedocs.io/en/latest/index.html).

Para o microframework flask é possível implantar em várias formas e modelos encontrados [aqui](https://flask.palletsprojects.com/en/1.1.x/deploying/). nessa documentação é possível configurar e utilizar em containers. Esses servidores são independentes quando são executados;

Seguindo a documentação do flask, foi feito o processo com o WSGI e depois foi transformado em docker.

### Configurando o uwsgi

Para o ***uwsgi*** é necessário que instale o pacote com o pip (com o *flask* já instalado). De preferência crie um ambiente virtual do `python3` (necessário o pacote deb `python3-venv`)

```bash
$ sudo apt install python3-venv
$ python3 -m venv /tmp/venv && source /tmp/venv/bin/activate
(venv) $ pip install flask uwsgi
```

Após isso pode-se iniciar o serviço com o comando abaixo.

```bash
(venv) $ uwsgi --http 127.0.0.1:5000 --module myproject:app
```

Isso iniciará o servidor em máquina local. Agora o propósito deste projeto é encapsular dentro de um *container* em *docker* para enfim ser implantado em produção. Baseado na documentação do [flask](https://flask.palletsprojects.com/en/1.1.x/deploying/uwsgi/) para esse processo e por questões de otimização, o processo será feito em um servidor nginx com o módulo e parâmetros do *uwsgi*.

Nesse processo, inicialmente será feito  um arquivo de configuração ***uwsgi.ini*** que receberá os parâmetros do servidor para executálo. Então inicie o processo criando o arquivo: `$ vim uwsgi.ini`

```bash
[uwsgi]
module = hello:app
uid = www-data
gid = www-data
master = true
processes = 5

socket = /tmp/uwsgi.socket
chmod-sock = 664
vacuum = true

die-on-term = true
```

A execução desse servidor se dará pelo comando `uwsgi --ini uwsgi.ini` que iniciará com 5 processos (`processes = 5`) garantindo dessa forma maior número de solicitações simultâneas. Por padrão se todos os cinco processos estiverem ocupados, ou seja, todos os workers estejam com pedidos de requisição, os próximos pedidos ficarão em uma fila que por padrão contém um tamanho de 100 pedidos. Nesse arquivo também é criado o arquivo de socket que deverá ser setado futuramente no arquivo do nginx.

Como os processos serão hospedados em um servidor da web e por padrão o usuário de servidor é `www-data` (em execução local, se for verificado o `uwsgi.socket` foi criado com usuário da máquina), é colocado no arquivo da mesma forma, para não ocorrer problemas de não pertencer ao usuário. 

### Configurando o Nginx

O proximo passo é criar o arquivo do nginx: `nginx.conf` para funcionar como um proxy para o servidor *uwsgi* que receberá as solicitações e encaminhará para o socket. Crie o arquivo e copie o seguinte para ele. `$ vim nginx.conf`

```bash
user www-data;
worker_processes auto;
pid /run/nginx.pid;

events {
    worker_connections 1024;
    use epoll;
    multi_accept on;
}

http {
    access_log /dev/stdout;
    error_log /dev/stdout;

    sendfile            on;
    tcp_nopush          on;
    tcp_nodelay         on;
    keepalive_timeout   65;
    types_hash_max_size 2048;

    include             /etc/nginx/mime.types;
    default_type        application/octet-stream;

    index   index.html index.htm;

    server {
        listen       80 default_server;
        listen       [::]:80 default_server;
        server_name  localhost;
        root         /var/www/html;

        location / {
            include uwsgi_params;
            uwsgi_pass unix:/tmp/uwsgi.socket;
        }
    }
}
```

No arquivo os logs gerados serão direcionados para stdout que poderão ser exibidos pelo comando `docker logs`  para verificações de erros e acessos.

Existe outra forma de realizar esse processo, ao contrário de modificar o `nginx.conf`, cria-se um arquivo de configuração em `/etc/nginx/sites-available/myproject`, seguindo os processos de criação de sites padrão.

### Script de inicialização do container

Para execução no container cria-se um script de inicialização simples que será padrão e referenciado no Dockerfile. `$ vim start.sh`

```bash
#!/usr/bin/env bash
service nginx start
uwsgi --ini uwsgi.ini
```

### Pacotes Python necessários

Para instalar e carregar os pacotes e dependências do servidor uwsgi deve-se criar um requirements que será carregado no Dockerfile.

```bash
(venv) $ pip freeze > requirements.txt
```

### Configurando Dockerfile

Agora criamos o Dockerfile que cria-rá a imagem docker do que já vimos até aqui.

```bash
 $ vim Dockerfile
```

```docker
FROM python:3.7-slim

RUN apt-get update &&  apt-get install -y \
    nginx \
    python3-dev \
    build-essential \
   && rm -rf /var/lib/apt/lists/*

WORKDIR /srv/flask_wsgi_docker
COPY ./ /srv/flask_wsgi_docker
COPY nginx.conf /etc/nginx

RUN pip install -r requirements.txt 
RUN chmod +x ./start.sh

CMD ["./start.sh"]
```

### Construindo a imagem e executando o container

Bem, para construir a imagem é necessário apenas executar o comando abaixo e setar a tag da imagem para executar depois.

```bash
$ docker build -t nome_da_imagem:tag_img .
```

Se o seu Dockerfile contém um nome distinto do padrão, deve-se referenciar o arquivo com `-f`

Após isso, será criado a imagem docker com o nome designado, para visualizar sua imagem execute um `docker images`

E por fim, execute o container:

```bash
$ docker run --rm -p 80:80 --name flask_uwsgi nome_da_imagem:tag
```

Por fim é só abrir no navegador em [localhost](http://localhost) para visualizar a saída da sua API
