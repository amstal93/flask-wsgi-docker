FROM python:3.7-slim

RUN apt-get update &&  apt-get install -y \
    nginx \
    python3-dev \
    build-essential \
   && rm -rf /var/lib/apt/lists/*

WORKDIR /srv/flask_wsgi_docker
COPY ./ /srv/flask_wsgi_docker
COPY nginx.conf /etc/nginx

RUN pip install -r requirements.txt 
RUN chmod +x ./start.sh

CMD ["./start.sh"]
